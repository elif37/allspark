<?php
include_once 'config.php';
include_once 'JWT.php';
use allspark\JWT\JWT;

if (isset($_COOKIE['token']))
	$token = $_COOKIE['token'];
else
{
	http_response_code(401);
	die(json_encode(array("code" => 401, "message" => "Access denied - No Token")));
}

try
{
	$payload = (new JWT($sha_key, 'HS512', 3600, 10))->decode($token);
}
catch (Throwable $e)
{
	http_response_code(401);
	die(json_encode(array("code" => 401, "message" => "Access denied - " . $e->getMessage())));
}
?>
