<?php
if (!preg_match("/^https:\/\/[a-z0-9]+.optimus-avocats.fr$/",@$_SERVER['HTTP_ORIGIN']))
{
	header("Access-Control-Allow-Origin: " . @$_SERVER['HTTP_ORIGIN']);
	header("Access-Control-Allow-Credentials: true");
	http_response_code(401);
	die(json_encode(array("code" => 401, "message" => "Les requêtes depuis " . @$_SERVER['HTTP_ORIGIN'] . " ne sont pas autorisées")));
}
header("Access-Control-Allow-Origin: " . (isset($_SERVER['HTTP_ORIGIN'])?$_SERVER['HTTP_ORIGIN']:$_SERVER['SERVER_NAME']));
header("Access-Control-Allow-Methods: GET, OPTIONS");
header("Access-Control-Allow-Credentials: true");
header("Access-Control-Allow-Headers: Content-Type, Accept, Authorization");
header("Access-Control-Max-Age: 5");
header("Content-Type: application/json");
if ($_SERVER['REQUEST_METHOD'] == "OPTIONS") die(http_response_code(200));

include_once 'auth.php';

function get_status($app)
{
	exec('systemctl status ' . $app, $output);
	foreach ($output as $line)
	if (strpos($line, 'Active: active') !== false)
		return 'active';
	return 'inactive';
}

function get_version($app)
{
  return shell_exec ("dpkg -s " . $app . " | grep '^Version:' | cut -c 10- | cut -f1 -d'-' | cut -f1 -d'+' | cut -f2 -d':' | head -c -1");
}

$status['services'][0]['type'] = 'Serveur web';
$status['services'][0]['name'] = 'Apache';
$status['services'][0]['version'] = get_version('apache2');
$status['services'][0]['status'] = get_status('apache2');

$status['services'][1]['type'] = 'Language de programmation';
$status['services'][1]['name'] = 'PHP';
$status['services'][1]['version'] = get_version('php');
$status['services'][1]['status'] = ($status['services'][1]['version'] != '')?'active':'inactive';

$status['services'][2]['type'] = 'Serveur de bases de données';
$status['services'][2]['name'] = 'Maria DB';
$status['services'][2]['status'] = get_status('mariadb');
$status['services'][2]['version'] = get_version('mariadb-server');

$status['services'][3]['type'] = 'Agent de transfert de courriels';
$status['services'][3]['name'] = 'Postfix';
$status['services'][3]['status'] = get_status('postfix');
$status['services'][3]['version'] = get_version('postfix');

$status['services'][4]['type'] = 'Serveur mail IMAP';
$status['services'][4]['name'] = 'Dovecot';
$status['services'][4]['status'] = get_status('dovecot');
$status['services'][4]['version'] = get_version('dovecot-core');

$status['services'][5]['type'] = 'Antispam Mail';
$status['services'][5]['name'] = 'SpamAssassin';
$status['services'][5]['status'] = get_status('spamassassin');
$status['services'][5]['version'] = get_version('spamassassin');

$status['services'][6]['type'] = 'Antispam Mail (connecteur)';
$status['services'][6]['name'] = 'Spamassassin Milter';
$status['services'][6]['status'] = get_status('spamass-milter');
$status['services'][6]['version'] = get_version('spamass-milter');

$status['services'][7]['type'] = 'Antivirus mail';
$status['services'][7]['name'] = 'Clamav Milter';
$status['services'][7]['status'] = get_status('clamav-milter');
$status['services'][7]['version'] = get_version('clamav-milter');

$status['services'][8]['type'] = 'Logiciel de sauvegardes différentielles';
$status['services'][8]['name'] = 'Rdiff Backup';
$status['services'][8]['status'] = get_status('rdiff-backup');
$status['services'][8]['version'] = get_version('rdiff-backup');

$status['services'][9]['type'] = 'Générateur de certificats SSL';
$status['services'][9]['name'] = 'Certbot';
$status['services'][9]['version'] = get_version('certbot');
$status['services'][9]['status'] = ($status['services'][9]['version'] != '')?'active':'inactive';
$status['services'][9]['other'] = "Expiration : " . date('d/m/Y',strtotime(substr(shell_exec("openssl s_client -servername '" . $_SERVER['HTTP_HOST'] . "' -connect '" . $_SERVER['HTTP_HOST'] . ":443' | openssl x509 -noout -enddate | grep 'notAfter='"),9)));

$status['services'][10]['type'] = 'Webmail';
$status['services'][10]['name'] = 'Roundcube Webmail';
$status['services'][10]['version'] = shell_exec ("cat /srv/webmail/CHANGELOG | grep 'RELEASE' | head -1 | cut -c 9- | head -c -1");
$status['services'][10]['status'] = ($status['services'][10]['version'] != '')?'active':'inactive';

$status['services'][11]['type'] = 'Serveur de fichiers WEBDAV';
$status['services'][11]['name'] = 'SabreDAV';
$status['services'][11]['version'] = shell_exec ("cat /srv/cloud/vendor/sabre/dav/lib/DAV/Version.php | grep VERSION | cut -f2 -d \"'\"");
$status['services'][11]['status'] = ($status['services'][11]['version'] != '')?'active':'inactive';

$status['services'][12]['type'] = 'Interface de Programmation';
$status['services'][12]['name'] = 'Optimus API';
$status['services'][12]['status'] = 'active';
exec('cat /srv/api/api_optimus/VERSION', $version);
$status['services'][12]['version'] = $version[1];
$status['services'][12]['other'] = $version[0];

$status['cpu']['usage'] =  floatval(shell_exec("top -n 1 -b | awk '/^%Cpu/{print $2}'"));
$status['memory']['usage'] = floatval(shell_exec("free | grep Mem | awk '{print $3/$2 * 100.0}'"));
$status['disk']['usage']['system'] = preg_split('/\s+/',shell_exec("df --output=source,target,used,size,pcent | grep ' / '"));
$status['disk']['usage']['data'] = preg_split('/\s+/',shell_exec("df --output=source,target,used,size,pcent | grep '/srv'"));

http_response_code(200);
die(json_encode(array("code" => 200, "data" => $status)));
?>
