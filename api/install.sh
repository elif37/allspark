#!/bin/bash
source /etc/allspark/functions.sh
if [ -z $DOMAIN ]; then require DOMAIN string "Veuillez indiquer votre nom de domaine :"; source /root/.allspark; fi
if [ -z $ALLSPARK_API_MARIADB_USER ]; then require ALLSPARK_API_MARIADB_USER string "Veuillez renseigner le nom de l'utilisateur MARIADB pour l'api ALLSPARK :"; source /root/.allspark; fi
if [ -z $ALLSPARK_API_MARIADB_PASSWORD ] || [ $ALLSPARK_API_MARIADB_PASSWORD = "auto" ]; then require ALLSPARK_API_MARIADB_PASSWORD password "Veuillez renseigner le mot de passe de l'utilisateur MARIADB pour l'api ALLSPARK :"; source /root/.allspark; fi
if [ -z $AES_KEY ] || [ $AES_KEY = "auto" ]; then require AES_KEY aeskey "Veuillez renseigner une clé de chiffrement AES de 16 caractères [A-Za-z0-9]"; source /root/.allspark; fi
if [ -z $API_SHA_KEY ] || [ $API_SHA_KEY = "auto" ]; then require API_SHA_KEY aeskey "Veuillez renseigner une clé de chiffrement SHA de 16 caractères [A-Za-z0-9]"; source /root/.allspark; fi
if [ -z $MODULE_API ]; then require MODULE_API yesno "Voulez-vous installer l'espace d'hébergement api.$DOMAIN ?"; source /root/.allspark; fi
source /root/.allspark

if [ $MODULE_API = "Y" ]
then
  echo
  echo_green "==== INSTALLATION DE L'ESPACE D'HERGEMENT API ===="

  echo_magenta "Création de l'espace d'hébergement api.$DOMAIN..."
  if [ ! -d "/srv/api" ]; then verbose mkdir /srv/api; fi
  if [ ! -f "/etc/apache2/sites-enabled/api.conf" ]; then sed -e 's/%DOMAIN%/'$DOMAIN'/g' /etc/allspark/api/vhost > /etc/apache2/sites-enabled/api.conf; fi
  mkdir -p /srv/api/tmp
  chown -R www-data:www-data /srv/api

  cd /srv/api
  cp -R /etc/allspark/api/install/. /srv/api/
  envsubst '${ALLSPARK_API_MARIADB_USER} ${ALLSPARK_API_MARIADB_PASSWORD} ${DOMAIN} ${AES_KEY} ${API_SHA_KEY}' < /etc/allspark/api/install/config.php > /srv/api/config.php
  
  echo_magenta "Creation de l'utilisateur MARIADB"
  verbose mariadb -u root -e "GRANT SELECT ON users.users TO '$ALLSPARK_API_MARIADB_USER'@'localhost' IDENTIFIED BY '$ALLSPARK_API_MARIADB_PASSWORD';"
  verbose mariadb -u root -e "FLUSH PRIVILEGES;"

  echo_magenta "Redémarrage des services"
  verbose systemctl restart apache2
fi
